# iOS Development Submissions, Spring 2017.

To submit, please follow the following one-time setup:

1. Fork this repository.

    ![Fork repo](images/fork_repo.png)

1. Pick the defaults. You should now have a copy of this repository on your
   *Bitbucket* account. The name should also be maintained (i.e.
   `gitsubmissions`).

    ![Fork repo defaults](images/fork_repo_defaults.png)

1. Clone this repository to your local machine. In your terminal, type this
   replacing `username` with your Bitbucket username:

    ```
    git clone https://username@bitbucket.org/username/gitsubmissions
    ```

1. Change into the newly created directory with this repo by typing `cd
   gitsubmissions`. Then make sure *Git* is setup correctly by running the
   provided setup script. You can do that by typing in the terminal:

    ```
    make
    ```

1. You should be all set to start working. Any new Xcode project or Playground,
   add it to the directory named `gitsubmissions` that is generated after
   running `git clone`. This is the same directory you were located at after you
   typed `cd gitsubmissions` in the previous step.

## The workflow.

For every task you submit, do the following:

1. Create your Xcode project or Playground, making sure you pick not to create a
   **Git** repository along with it (the option will show up when creating a new
   Xcode project):

   ![No Git](images/no_git.png)

1. Once you're finished with your work. Save everything and go to your terminal.
   Make sure you're located at your `gitsubmissions` directory. i.e.

    ```
    cd gitsubmissions
    ```

    Notice that your `gitsubmissions` repository on your local machine may be
    located in a different place depending where you ran `git clone`.

1. Run the shortcut command to create branch, add, commit and push changes. If
   this is your first time to submit this branch:

    ```
    git submit username-branchname "Comment about this."
    ```

    If this is a resubmission then:

    ```
    git resubmit username-branchname "Comment about this."
    ```

1. Create a **Pull Request** in Bitbucket.

    ![Pull Requests](images/pull_request.png)

1. Make sure everything is right with your **Pull Request**:

    Note that in the screenshot provided, the name of the branch is
    `my-playground-branch`. Make sure `my` is replaced with your username
    instead, and `playground-branch` with the branch name required by the task.

    1. Should read your branch name.
    2. Should be the "master" repo, or `wnmiosdev/gitsubmissions`.
    3. Should read your branch name, new branch is what we want.
    4. The title should explain shortly what you did.
    5. The description is optional, good for extra information about it.
    6. Type `jglievano` as reviewer.
    7. Leave **unchecked**. We don't want your branch name to close.
    8. Create pull request.

    ![Pull Request Form](images/pull_request_form.png)