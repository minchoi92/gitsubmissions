#!/usr/bin/env python

import os
import os.path
import sys

def main():
    # Check if ~/.gitconfig exists.
    if os.path.isfile(os.path.expanduser('~/.gitconfig')):
        rewrite = ''
        while rewrite != 'y' and rewrite != 'n':
            rewrite = raw_input('~/.gitconfig already exists. Rewrite? [Yn]: ').lower()
        if rewrite == "n":
            print 'Nothing to do here then. Exiting.'
            sys.exit(0)

    # Prompt for name and email.
    name = raw_input('Please enter your full name: ')
    email = raw_input('Please enter your email: ')

    # Get current project location.
    dir_path = os.getcwd()

    # Read from hgrc template and substitute tags.
    with open('git/gitconfig', 'r') as content_file:
        content = content_file.read()
        content = content.replace('{{name}}', name)
        content = content.replace('{{email}}', email)
        with open(os.path.expanduser('~/.gitconfig'), 'wt') as fout:
            fout.write(content)

    print "~/.gitconfig generated. Setup completed!"

if __name__ == "__main__":
    main()
